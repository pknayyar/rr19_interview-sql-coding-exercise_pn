package com.sql.application.model;
/*
 * Invoice Model Class
 */
public class Invoice {
	// The Invoice Id
	private int id;
	// The Customer Name
	private String customerName;	
	// The Item
	private int item;
	// The Quantity
	private int quantity;
	// The Product Name
	private String productName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getItem() {
		return item;
	}
	public void setItem(int item) {
		this.item = item;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@Override
	public String toString() {
		return "Invoice [id=" + id + ", customerName=" + customerName + ", item=" + item + ", quantity=" + quantity
				+ ", productName=" + productName + "]";
	}	
}
