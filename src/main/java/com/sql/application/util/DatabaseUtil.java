package com.sql.application.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.sql.application.model.Invoice;

/*
 * Database Util class
 */
public class DatabaseUtil {
	// The Logger
	private final static Logger logger = Logger.getLogger(DatabaseUtil.class);
	// The Connection
	private static Connection connection = null;
	
	/*
	 * Get Invoice Details By Id
	 */
	public static List<Invoice> getInvoiceDetailsById(String id) {
		String query = PropertyReader.queryForGetDetailsByInvoiceId(id);
		logger.info("query "+query);
		
		ResultSet rs = getResultSet(query);
		logger.info("rs "+rs);
		
		List<Invoice> invoices = new ArrayList<Invoice>();
		try {
			while(rs.next()) {
				Invoice invoice = new Invoice();
				invoice.setId(rs.getInt(1));
				invoice.setCustomerName(rs.getString(2)+" "+rs.getString(3));
				invoice.setItem(rs.getInt(4));
				invoice.setQuantity(rs.getInt(5));
				invoice.setProductName(rs.getString(6));
				invoices.add(invoice);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return invoices;
	}
	
	/*
	 * Get Customer Id By Customer Name
	 */
	public static int getCustomerIdByName(String firstName, String lastName) {
		String query = PropertyReader.queryForGetCustomerIdByName(firstName, lastName);
		logger.info("query "+query);
		
		ResultSet rs = getResultSet(query);
		logger.info("rs "+rs);	
		
		try {
			if(rs.next())
				return rs.getInt(1);				
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/*
	 * Get Product Id By Name And Price
	 */
	public static int getProductIdByNameAndPrice(String productName, double price) {
		String query = PropertyReader.queryForGetProductIdByNameAndPrice(productName, price);
		logger.info("query "+query);
		
		ResultSet rs = getResultSet(query);
		logger.info("rs "+rs);	
		
		try {
			if(rs.next())
				return rs.getInt(1);				
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/*
	 * Create new Invoice
	 */
	public static int insertIntoInvoice(int invoiceId, int customerId, double total) {
		String query = PropertyReader.queryForInsertIntoInvoice(invoiceId, customerId, total);
		logger.info("query "+query);
		
		int getUpdatedCount = getUpdatedCount(query);
		logger.info("getUpdatedCount "+getUpdatedCount);	
		
		return getUpdatedCount;
	}
	
	/*
	 * Create new Item
	 */
	public static int insertIntoItem(int invoiceId, int item, int productId, int quantity, double cost) {
		String query = PropertyReader.queryForInsertIntoItems(invoiceId, item, productId, quantity, cost);
		logger.info("query "+query);
		
		int getUpdatedCount = getUpdatedCount(query);
		logger.info("getUpdatedCount "+getUpdatedCount);	
		
		return getUpdatedCount;
	}
	
	/*
	 * Get max id from Invoice
	 */
	public static int getMaxIdFromInvoice() {
		String query = PropertyReader.queryForGetMaxIdFromInvoice();
		logger.info("query "+query);
		
		ResultSet rs = getResultSet(query);
		logger.info("rs "+rs);	
		
		try {
			if(rs.next())
				return rs.getInt(1);				
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/*
	 * Get new SQL Connection
	 */
	private static Connection getConnection() {
		try {
        	if(connection != null)
        		return connection;
            Class.forName(PropertyReader.getDriver());
            connection = DriverManager.getConnection(PropertyReader.getUrl(), PropertyReader.getUserName(), PropertyReader.getPassword());
            logger.info("Connection created!");
            return connection;          
        }
        catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
		return connection;       
	}
	
	/*
	 * Get Result Set
	 */
	private static ResultSet getResultSet(String query) {		
        try {
           Connection connection = getConnection();  
           Statement statement = connection.createStatement();
           ResultSet rs =  statement.executeQuery(query);
           return rs;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
	}
	
	/*
	 * Get Updated Count for the Query
	 */
	private static int getUpdatedCount(String query) {		
        try {
           Connection connection = getConnection();  
           Statement statement = connection.createStatement();
           int count =  statement.executeUpdate(query);
           return count;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
	}
}
