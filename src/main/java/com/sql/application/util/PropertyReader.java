package com.sql.application.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
/*
 * The Property Reader Class
 */
public class PropertyReader {
	private static Properties properties;
	static {
		if(properties== null) {
			properties = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = loader.getResourceAsStream("config.properties");
			try {
				properties.load(inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public static String getInvoiceId() {
		return properties.getProperty("invoice.id");
	}
	
	public static int getRowsCount() {
		return Integer.parseInt(properties.getProperty("rows.count"));
	}
	
	public static int getCustomerId() {
		return Integer.parseInt(properties.getProperty("customer.id"));
	}
	
	public static String getCustomerName() {
		return properties.getProperty("customer.name");
	}
	
	public static List<String> getItems() {
		return Arrays.asList(properties.getProperty("items").split(","));
	}
	
	public static List<String> getQuantities() {
		return Arrays.asList(properties.getProperty("quantities").split(","));
	}
	
	public static List<String> getProductNames() {
		return Arrays.asList(properties.getProperty("product.names").split(","));
	}
	
	public static String queryForGetDetailsByInvoiceId(String id) {
		return MessageFormat.format(properties.getProperty("find.details.by.invoice.id"), id);
	}
	
	public static String queryForGetCustomerIdByName(String firstName, String lastName) {
		return MessageFormat.format(properties.getProperty("find.customer.id.by.name"), firstName, lastName);
	}
	
	public static String queryForInsertIntoInvoice(int invoiceId, int customerId, double total) {
		return MessageFormat.format(properties.getProperty("insert.into.invoice"), invoiceId, customerId, total);
	}
	
	public static String queryForGetProductIdByNameAndPrice(String productName, double price) {
		return MessageFormat.format(properties.getProperty("find.product.id.by.name.and.price"), productName, price);
	}
	
	public static String queryForInsertIntoItems(int invoiceId, int item, int productId, int quantity, double cost) {
		return MessageFormat.format(properties.getProperty("insert.into.items"), invoiceId, item, productId, quantity, quantity*cost);
	}
	
	public static String queryForGetMaxIdFromInvoice() {
		return properties.getProperty("max.id.in.invoice");
	}

	public static String getNewCustomer() {
		return properties.getProperty("new.customer.name");
	}
	
	public static List<String> getNewProducts() {
		return Arrays.asList(properties.getProperty("new.products").split(","));
	}	

	public static List<String> getNewProductsCosts() {
		return Arrays.asList(properties.getProperty("new.products.costs").split(","));
	}
	
	public static String getQuantity() {
		return properties.getProperty("quantity");
	}
	
	public static String getDriver() {
		return properties.getProperty("driver");
	}
	
	public static String getUrl() {
		return properties.getProperty("url");
	}
	
	public static String getUserName() {
		return properties.getProperty("username");
	}
	
	public static String getPassword() {
		return properties.getProperty("password");
	}	
}
