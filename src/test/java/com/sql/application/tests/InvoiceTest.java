package com.sql.application.tests;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import com.sql.application.model.Invoice;
import com.sql.application.util.AppUtil;
import com.sql.application.util.DatabaseUtil;
import com.sql.application.util.PropertyReader;

public class InvoiceTest {
	
	/*
	 * Test invoice details such as items, quantity, price, product name and etc by given invoice Id
	 */
	@Test
	public void testInvoiceDetailsById() {
		String invoiceId = PropertyReader.getInvoiceId();
		int expectedRows = PropertyReader.getRowsCount();
		int customerId = PropertyReader.getCustomerId();
		String customerName = PropertyReader.getCustomerName();
		List<String> items = PropertyReader.getItems();
		List<String> quantities = PropertyReader.getQuantities();
		List<String> productNames = PropertyReader.getProductNames();
		verifyInvoice(invoiceId, expectedRows, customerId, customerName, items, quantities, productNames);		
	}
	
	/*
	 * Test creating new invoice and verify created invoice details such as items, quantity, price, product name and etc by given invoice Id
	 */
	@Test
	public void testCreateInvoice() {
		List<String> items = new ArrayList<String>();
		List<String> quantities = new ArrayList<String>();
		String customerName = PropertyReader.getNewCustomer();
		List<String> products = PropertyReader.getNewProducts();
		List<String> costs = PropertyReader.getNewProductsCosts();
		int invoiceId = DatabaseUtil.getMaxIdFromInvoice()+1;
		String quantity = PropertyReader.getQuantity();
		int customerId = DatabaseUtil.getCustomerIdByName(customerName.split(" ")[0], customerName.split(" ")[1]);
		DatabaseUtil.insertIntoInvoice(invoiceId, customerId, AppUtil.totalCosts(costs));
		for(int i=0; i<products.size(); i++) {
			int productId = DatabaseUtil.getProductIdByNameAndPrice(products.get(i), Double.parseDouble(costs.get(i)));
			DatabaseUtil.insertIntoItem(invoiceId, i, productId, Integer.parseInt(quantity), Double.parseDouble(costs.get(i)));
			items.add(String.valueOf(i));
			quantities.add(quantity);
		}	
		
		verifyInvoice(String.valueOf(invoiceId), products.size(), customerId, customerName, items, quantities, products);	
	}
	
	/*
	 * Verify Invoice using possible assertions
	 */
	private void verifyInvoice(String invoiceId, int expectedRows, int customerId, String customerName, List<String> items, List<String> quantities, List<String> productNames) {
		List<Invoice> invoices = DatabaseUtil.getInvoiceDetailsById(invoiceId);
		
		assertEquals(invoices.size(), expectedRows, "Rows Count Should Be "+expectedRows);
		for(int i=0; i<invoices.size(); i++) {
			assertEquals(invoices.get(i).getId(), customerId, "Customer Id Should Be "+customerName);
			assertEquals(invoices.get(i).getCustomerName(), customerName, "Customer Name Should Be "+customerName);
			assertEquals(invoices.get(i).getItem(), Integer.parseInt(items.get(i)), "Item Should Be "+items.get(i));
			assertEquals(invoices.get(i).getQuantity(), Integer.parseInt(quantities.get(i)), "Quantity Should Be "+quantities.get(i));
			assertEquals(invoices.get(i).getProductName(), productNames.get(i), "Product Name Should Be "+productNames.get(i));
		}		
	}	
}